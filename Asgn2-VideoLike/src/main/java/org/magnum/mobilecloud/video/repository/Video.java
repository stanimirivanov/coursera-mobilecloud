package org.magnum.mobilecloud.video.repository;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.common.base.Objects;

/**
 * A simple object to represent a video and its URL for viewing.
 * 
 * You probably need to, at a minimum, add some annotations to this
 * class.
 * 
 * You are free to add annotations, members, and methods to this
 * class. However, you probably should not change the existing
 * methods or member variables. If you do change them, you need
 * to make sure that they are serialized into JSON in a way that
 * matches what is expected by the auto-grader.
 * 
 * @author mitchell
 */
/**
 * @author SI80705
 *
 */
@Entity
public class Video {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String name;
	private String url;
	private long duration;
	
	@ElementCollection
	private Set<String> likesUsernames;

	public Video() {
	}

	public Video(String name, String url, long duration, long likes) {
		super();
		this.name = name;
		this.url = url;
		this.duration = duration;
		this.likesUsernames = new HashSet<String>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getLikes() {
		return likesUsernames.size();
	}
	
	public Set<String> getLikesUsernames() {
		return likesUsernames;
	}

	public void setLikesUsernames(Set<String> likesUsernames) {
		this.likesUsernames = likesUsernames;
	}

	/**
	 * User likes this video.
	 * 
	 * @param username the username for the person who likes this video
	 * 
	 * @throws IllegalArgumentException if user already has liked this video
	 */
	public void like(String username) throws IllegalArgumentException {
		Set<String> likesUsernames = getLikesUsernames();
		if (likesUsernames.contains(username)) {
			throw new IllegalArgumentException("User already liked this video.");
		} else {
			likesUsernames.add(username);
			setLikesUsernames(likesUsernames);
		}
	}
	
	/**
	 * User unlikes this video.
	 * 
	 * @param username the username for the person who unlikes this video
	 * 
	 * @throws IllegalArgumentException if user had not liked this this video before
	 */
	public void unlike(String username) throws IllegalArgumentException {
		Set<String> likesUsernames = getLikesUsernames();
		if (likesUsernames.contains(username)) {
			likesUsernames.remove(username);
			setLikesUsernames(likesUsernames);
		} else {
			throw new IllegalArgumentException("User had not liked this this video before.");
		}
	}
	
	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(name, url, duration);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Video) {
			Video other = (Video) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(name, other.name)
					&& Objects.equal(url, other.url)
					&& duration == other.duration;
		} else {
			return false;
		}
	}

}
