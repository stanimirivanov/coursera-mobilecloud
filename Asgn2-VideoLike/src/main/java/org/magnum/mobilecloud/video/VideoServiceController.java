/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.magnum.mobilecloud.video;

import java.security.Principal;
import java.util.Collection;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.video.repository.Video;
import org.magnum.mobilecloud.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;

@Controller
public class VideoServiceController {
	
	/**
	 * You will need to create one or more Spring controllers to fulfill the
	 * requirements of the assignment. If you use this file, please rename it
	 * to something other than "AnEmptyController"
	 * 
	 * 
		 ________  ________  ________  ________          ___       ___  ___  ________  ___  __       
		|\   ____\|\   __  \|\   __  \|\   ___ \        |\  \     |\  \|\  \|\   ____\|\  \|\  \     
		\ \  \___|\ \  \|\  \ \  \|\  \ \  \_|\ \       \ \  \    \ \  \\\  \ \  \___|\ \  \/  /|_   
		 \ \  \  __\ \  \\\  \ \  \\\  \ \  \ \\ \       \ \  \    \ \  \\\  \ \  \    \ \   ___  \  
		  \ \  \|\  \ \  \\\  \ \  \\\  \ \  \_\\ \       \ \  \____\ \  \\\  \ \  \____\ \  \\ \  \ 
		   \ \_______\ \_______\ \_______\ \_______\       \ \_______\ \_______\ \_______\ \__\\ \__\
		    \|_______|\|_______|\|_______|\|_______|        \|_______|\|_______|\|_______|\|__| \|__|
                                                                                                                                                                                                                                                                        
	 * 
	 */
	
	@Autowired
	private VideoRepository videoRepository;
	
	/**
	 * Returns all videos as JSON objects.
	 * 
	 * @return collection of {@link Video} objects
	 */
	@RequestMapping(value = "/video", method = RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList() {
		return Lists.newArrayList(videoRepository.findAll());
	}
	
	/**
	 * Gets a video with a specified id
	 * 
	 * @param id the video id to get
	 * @param response an http servlet response
	 * 
	 * @return <code>null</code> if no video found, in that case http response is set to 404
	 */
	@RequestMapping(value = "/video/{id}", method = RequestMethod.GET)
	public @ResponseBody Video getVideoById(@PathVariable("id") long id, HttpServletResponse response) {
		Video video = videoRepository.findOne(id);
		if (video == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		return video;
	}
	
	/**
	 * Adds the video. Returns the saved video represented as JSON
	 * 
	 * @param video the video to add
	 * 
	 * @return a video object
	 */
	@RequestMapping(value = "/video", method = RequestMethod.POST)
	public @ResponseBody Video addVideo(@RequestBody Video video) {
		return videoRepository.save(video);
	}
	
	/**
	 * Allows a user to like a video. 
	 * Return 200 Ok on success, 404 if the video is not found, 
	 * or 400 if the user has already liked the video.
	 * 
	 * @param id video id to like
	 * @param principal security principal
	 * @param response an http servlet response
	 */
	@RequestMapping(value = "/video/{id}/like", method = RequestMethod.POST)
	public void likeVideo(@PathVariable("id") long id, Principal principal, HttpServletResponse response) {
		Video video = getVideoById(id, response);

		if (video != null) {
			
			try {
				video.like(principal.getName());
				videoRepository.save(video);
				response.setStatus(HttpServletResponse.SC_OK);
			} catch (IllegalArgumentException  e) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		}
	}
	
	/**
	 * Allows a user to unlike a video. 
	 * Return 200 Ok on success, 404 if the video is not found, 
	 * or 400 if the user has not previously liked the video.
	 * 
	 * @param id video id to unlike
	 * @param principal security principal
	 * @param response an http servlet response
	 */
	@RequestMapping(value = "/video/{id}/unlike", method = RequestMethod.POST)
	public void unlikeVideo(@PathVariable("id") long id, Principal principal, HttpServletResponse response) {
		Video video = getVideoById(id, response);

		if (video != null) {
			
			try {
				video.unlike(principal.getName());
				videoRepository.save(video);
				response.setStatus(HttpServletResponse.SC_OK);
			} catch (IllegalArgumentException  e) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		}
	}

	/**
	 * Returns a list of videos whose titles match the given parameter or an empty 
	 * list if none are found.
	 * 
	 * @param title the video title to find
	 * 
	 * @return empty list if none found
	 */
	@RequestMapping(value = "/video/search/findByName", method = RequestMethod.GET)
	public @ResponseBody Collection<Video> findByTitle(@RequestParam("title") String title) {
		Collection<Video> videos = videoRepository.findByName(title);
		if (videos == null) {
			videos = Lists.newArrayList();
		}
		return videos;
	}
	
	/**
	 * Returns a list of videos whose durations are less than the given parameter 
	 * or an empty list if none are found.
	 * 
	 * @param duration the video duration to find by
	 * 
	 * @return empty list if none found
	 */
	@RequestMapping(value = "/video/search/findByDurationLessThan", method = RequestMethod.GET)
	public @ResponseBody Collection<Video> findByDurationLessThan(@RequestParam("duration") long duration) {
		Collection<Video> videos = videoRepository.findByDurationLessThan(duration);
		if (videos == null) {
			videos = Lists.newArrayList();
		}
		return videos;
	}
	
	/**
	 * Returns a list of the string usernames of the users that have liked the 
	 * specified video. 
	 * If the video is not found, a 404 error should be generated.
	 * 
	 * @param id the video id to get the users that who have liked it
	 * @param response an http servlet response
	 * 
	 * @return
	 */
	@RequestMapping(value = "/video/{id}/likedby", method = RequestMethod.GET)
	public @ResponseBody Set<String> getUsersWhoLikedVideo(@PathVariable("id") long id, HttpServletResponse response) {
		Video video = getVideoById(id, response);		
		return video.getLikesUsernames();
	}
	
}
