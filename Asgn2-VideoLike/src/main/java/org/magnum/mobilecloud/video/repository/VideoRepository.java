package org.magnum.mobilecloud.video.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * An interface for a repository that can store Video
 * objects and allow them to be searched by title.
 *
 * This @RepositoryRestResource annotation tells Spring Data Rest to
 * expose the VideoRepository through a controller and map it to the 
 * "/video" path. This automatically enables you to do the following:
 * 
 * 1. List all videos by sending a GET request to /video 
 * 2. Add a video by sending a POST request to /video with the JSON for a video
 * 3. Get a specific video by sending a GET request to /video/{videoId}
 *    (e.g., /video/1 would return the JSON for the video with id=1)
 * 4. Send search requests to our findByXYZ methods to /video/search/findByXYZ
 *    (e.g., /video/search/findByName?title=Foo)
 */
@RepositoryRestResource(path = "/video")
public interface VideoRepository extends CrudRepository<Video, Long>{
	
	/**
	 * Find all videos with a matching title (e.g., Video.name)
	 * 
	 * The @Param annotation tells Spring Data Rest which HTTP request 
	 * parameter it should use to fill in the "title" variable used to
	 * search for Videos
	 * 
	 * @param title video's name
	 * 
	 * @return collection of Video objects
	 */
	public Collection<Video> findByName(@Param("title") String title);
	
	/**
	 * Find all videos that are shorter than a specified duration.
	 * 
	 * The @Param annotation tells tells Spring Data Rest which HTTP request
	 * parameter it should use to fill in the "duration" variable used to
	 * search for Videos
	 * 
	 * @param duration video's duration
	 * 
	 * @return collection of Video objects
	 */
	public Collection<Video> findByDurationLessThan(@Param("duration") long duration);
	
}
