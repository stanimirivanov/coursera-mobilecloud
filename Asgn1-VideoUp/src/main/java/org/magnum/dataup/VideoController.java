/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package org.magnum.dataup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.magnum.dataup.model.Video;
import org.magnum.dataup.model.VideoStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class VideoController {

	/**
	 * You will need to create one or more Spring controllers to fulfill the
	 * requirements of the assignment. If you use this file, please rename it
	 * to something other than "AnEmptyController"
	 * 
	 * 
		 ________  ________  ________  ________          ___       ___  ___  ________  ___  __       
		|\   ____\|\   __  \|\   __  \|\   ___ \        |\  \     |\  \|\  \|\   ____\|\  \|\  \     
		\ \  \___|\ \  \|\  \ \  \|\  \ \  \_|\ \       \ \  \    \ \  \\\  \ \  \___|\ \  \/  /|_   
		 \ \  \  __\ \  \\\  \ \  \\\  \ \  \ \\ \       \ \  \    \ \  \\\  \ \  \    \ \   ___  \  
		  \ \  \|\  \ \  \\\  \ \  \\\  \ \  \_\\ \       \ \  \____\ \  \\\  \ \  \____\ \  \\ \  \ 
		   \ \_______\ \_______\ \_______\ \_______\       \ \_______\ \_______\ \_______\ \__\\ \__\
		    \|_______|\|_______|\|_______|\|_______|        \|_______|\|_______|\|_______|\|__| \|__|
                                                                                                                                                                                                                                                                        
	 * 
	 */
	
	@Autowired
	private VideoFileManager fileManager;
	
	private static final AtomicLong ID = new AtomicLong(0L);
	
	private Map<Long,Video> videos = new HashMap<Long, Video>();
	
	/**
	 * Handles the <code>getVideoList()</code> request.
	 * 
	 * @return list of {@link Video} objects
	 */
	@RequestMapping(value = "/video", method = RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList() {
		Collection<Video> videoList = new ArrayList<Video>();
		for (long id: videos.keySet())
			videoList.add(videos.get(id));
		
		return videoList;
	}
	
	/**
	 * Handles the <code>addVideo(Video)</code> request.
	 * 
	 * @param v the video to add
	 * 
	 * @return {@link Video}
	 */
	@RequestMapping(value = "/video", method = RequestMethod.POST)
	public @ResponseBody Video addVideo(@RequestBody Video v) {
		if (v.getId() == 0) {
			long id = ID.incrementAndGet();
			v.setId(id);
		}
		
		HttpServletRequest request =
                ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String url = "http://" + request.getServerName() + ":" + request.getServerPort() + 
				"/video/" + v.getId() + "/data";
		v.setDataUrl(url);
		videos.put(v.getId(), v);
		return v;
	}
	
	/**
	 * Handles the <code>setVideoData(long, TypedFile)</code> request.
	 * 
	 * @param id the video id to add the data to
	 * @param videoData the video data to add
	 * @param response the servlet response
	 * 
	 * @return {@link VideoStatus}
	 */
	@RequestMapping(value = "/video/{id}/data", method = RequestMethod.POST)
	public @ResponseBody VideoStatus setVideoData(@PathVariable("id") long id,
			 @RequestParam("data") MultipartFile videoData, HttpServletResponse response) 
					 throws IOException {
		if (videos.containsKey(id)) {
			fileManager.saveVideoData(videos.get(id), videoData.getInputStream());
			VideoStatus status = new VideoStatus(VideoStatus.VideoState.READY);
			return status;
		} else {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
	}
	
	/**
	 * Handles the <code>getData(long)</code> request.
	 * 
	 * @param id the video id to get
	 * @param response the servlet response
	 */
	@RequestMapping(value = "/video/{id}/data", method = RequestMethod.GET)
    public void getData(@PathVariable("id") long id, HttpServletResponse response) 
    		throws IOException {
        Video v = videos.get(id);
        if (v != null) {
            fileManager.copyVideoData(v, response.getOutputStream());
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
	
}
